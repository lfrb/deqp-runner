use anyhow::{Context, Result};
use deqp_runner::deqp_command::DeqpCommand;
use deqp_runner::mock_deqp::{mock_deqp, MockDeqp};
use deqp_runner::{
    parallel_test, parse_regex_set, process_results, read_baseline, read_lines,
    CommandLineRunOptions, RunnerResults, TestCase, TestCommand,
};
use deqp_runner::{JunitGeneratorOptions, TestConfiguration};
use log::*;
use serde::Deserialize;
use std::collections::HashMap;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::time::Duration;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(
    author = "Emma Anholt <emma@anholt.net>",
    about = "Runs dEQP in parallel"
)]
struct Opts {
    #[structopt(subcommand)]
    subcmd: SubCommand,
}

#[derive(Debug, StructOpt)]
enum SubCommand {
    #[structopt(name = "run")]
    Run(Run),

    #[structopt(name = "junit")]
    Junit(Junit),

    #[structopt(name = "suite")]
    Suite(Suite),

    #[structopt(
        name = "mock-deqp",
        help = "deqp-runner internal mock deqp binary for testing"
    )]
    MockDeqp(MockDeqp),
}

#[derive(Debug, StructOpt)]
pub struct Run {
    #[structopt(long, help = "path to deqp binary")]
    deqp: PathBuf,

    #[structopt(long, help = "path to deqp caselist (such as *-mustpass.txt)")]
    caselist: Vec<PathBuf>,

    #[structopt(flatten)]
    common: CommandLineRunOptions,

    #[structopt(
        long,
        default_value = "500",
        help = "Starting number of tests to include in each deqp invocation (mitigates deqp startup overhead)"
    )]
    tests_per_group: usize,

    // The runner  corehas clever code I inherited from the parallel-deqp-runner
    // project to, near the end of the list of test of tests to shard out, make
    // smaller groups of tests so that you don't end up with a long test list
    // left in one deqp process limiting your total runtime.
    //
    // The problem that cleverness faces is this table of deqp startup time:
    //
    //             freedreno  radeon
    // deqp-gles2  0.2s       0.08s
    // deqp-vk     2.0s       0.6s
    //
    // Even if all the slowest tests I have (a few deqp-vks on radeon at 6-7s)
    // land in the same test list near the end of the run, you're still going to
    // end up spawning a ton of extra deqp processes trying to be clever, costing
    // more than your tests.
    //
    // So, disable it by default by using our normal group size the whole time
    // unless someone sets the option to something else.
    #[structopt(
        long,
        default_value = "0",
        help = "Minimum number of tests to scale down to in each deqp invocation (defaults to 0 to match tests_per_group)"
    )]
    min_tests_per_group: usize,

    #[structopt(
        long,
        help = "Optional path to executor/testlog-to-xml, for converting QPA files to usable XML"
    )]
    testlog_to_xml: Option<PathBuf>,

    #[structopt(
        long,
        default_value = "",
        help = "regex to match against the GL_RENDERER or VK deviceName"
    )]
    renderer_check: String,

    #[structopt(
        long,
        default_value = "",
        help = "regex to match against the GL_VERSION"
    )]
    version_check: String,

    #[structopt(help = "arguments to deqp binary")]
    deqp_args: Vec<String>,
}

#[derive(Debug, StructOpt)]
pub struct Junit {
    #[structopt(long, help = "Path to source results.csv or failures.csv")]
    results: PathBuf,

    #[structopt(long, short = "o", help = "Path to write junit XML to")]
    output: PathBuf,

    #[structopt(flatten)]
    junit_generator_options: JunitGeneratorOptions,
}

#[derive(Debug, StructOpt)]
pub struct Suite {
    #[structopt(long, help = "Path to suite.toml")]
    suite: PathBuf,

    #[structopt(flatten)]
    common: CommandLineRunOptions,

    #[structopt(
        long,
        help = "Optional path to executor/testlog-to-xml, for converting QPA files to usable XML"
    )]
    testlog_to_xml: Option<PathBuf>,
}

// Common structure for configuring a deqp run between Run (single run) and Suite (muliple Runs)
#[derive(Deserialize)]
struct DeqpConfig {
    deqp: PathBuf,

    caselists: Vec<PathBuf>,

    #[serde(default)]
    skips: Vec<PathBuf>,

    #[serde(default)]
    flakes: Vec<PathBuf>,

    #[serde(default)]
    baseline: Option<PathBuf>,

    #[serde(default)]
    include: Vec<String>,

    #[serde(default)]
    deqp_args: Vec<String>,

    #[serde(default)]
    timeout: f32,

    #[serde(default)]
    min_tests_per_group: usize,

    #[serde(default)]
    tests_per_group: usize,

    #[serde(default)]
    fraction: usize,

    #[serde(default)]
    fraction_start: usize,

    #[serde(default)]
    env: HashMap<String, String>,

    #[serde(default)]
    prefix: String,

    #[serde(default)]
    renderer_check: String,

    #[serde(default)]
    version_check: String,
}

impl DeqpConfig {
    fn deqp(
        &self,
        output_dir: &Path,
        qpa_to_xml: Option<PathBuf>,
        save_xfail_logs: bool,
    ) -> Result<DeqpCommand> {
        Ok(DeqpCommand {
            deqp: self.deqp.clone(),
            args: self.deqp_args.clone(),
            config: TestConfiguration {
                output_dir: output_dir.to_path_buf(),
                skips: parse_regex_set(read_lines(&self.skips)?)?,
                flakes: parse_regex_set(read_lines(&self.flakes)?)?,
                baseline: read_baseline(self.baseline.as_ref())?,
                timeout: Duration::from_secs_f32(self.timeout),
                env: self.env.clone(),
                save_xfail_logs,
            },
            qpa_to_xml,
            prefix: self.prefix.to_owned(),
        })
    }

    fn test_groups<'d>(
        &self,
        deqp: &'d DeqpCommand,
        filters: &[String],
    ) -> Result<Vec<(&'d DeqpCommand, Vec<TestCase>)>> {
        if self.tests_per_group < 1 {
            eprintln!("tests_per_group must be >= 1.");
            std::process::exit(1);
        }

        let rayon_threads = rayon::current_num_threads();

        let mut include_filters = Vec::new();
        if !self.include.is_empty() {
            include_filters
                .push(parse_regex_set(&self.include).context("compiling include filters")?);
        }
        if !filters.is_empty() {
            include_filters.push(parse_regex_set(filters).context("compiling include filters")?);
        }

        let test_names: Vec<TestCase> = read_lines(&self.caselists)?
            .into_iter()
            .map(TestCase::Deqp)
            .skip(self.fraction_start - 1)
            .step_by(self.fraction)
            .filter(|test| include_filters.iter().all(|x| x.is_match(test.name())))
            .collect::<Vec<TestCase>>();

        if !test_names.is_empty()
            && (!self.renderer_check.is_empty() || !self.version_check.is_empty())
        {
            // Look at the testcases in the caselist to decide how to probe the
            // renderer.  Note that we do some inference, because the caselist
            // might not contain dEQP-GLESn.info.renderer or whatever in it.
            //
            // The alternative would be to use the testcase binary to infer, but
            // then we don't know if we should be printing
            // KHR-GLES32.info.renderer or KHR-GL33.info.renderer or whatever.
            let deqp_version = test_names[0]
                .name()
                .split('.')
                .next()
                .context("finding dEQP case prefix")?;

            let (renderer_testcase, version_testcase) = match deqp_version {
                "dEQP-VK" => ("dEQP-VK.info.device".to_owned(), "".to_owned()),
                "KHR-GLES2" | "KHR-GLES3" | "KHR-GLES31" | "dEQP-EGL" => {
                    anyhow::bail!(
                        "Can't do a renderer check based on testcase name {}",
                        test_names[0].name()
                    );
                }
                _ => (
                    format!("{}.info.renderer", &deqp_version),
                    format!("{}.info.version", &deqp_version),
                ),
            };

            let default_regex = ".*";

            let renderer_check = if self.renderer_check.is_empty() {
                default_regex
            } else {
                &self.renderer_check
            };
            let version_check = if self.renderer_check.is_empty() {
                default_regex
            } else {
                &self.version_check
            };

            // Make sure that we test (and thus log) both renderer and version
            // before exiting out due to a mismatch.
            let renderer_ok = deqp
                .qpa_text_check(&renderer_testcase, renderer_check, "renderer")
                .context("checking renderer")?;
            let version_ok = version_testcase.is_empty()
                || deqp
                    .qpa_text_check(&version_testcase, version_check, "version")
                    .context("checking version")?;

            if !renderer_ok {
                error!("Renderer mismatch ({})", &self.renderer_check);
                std::process::exit(1);
            }

            if !version_ok {
                error!("Version mismatch ({})", &self.version_check);
                std::process::exit(1);
            }
        }

        let groups =
            deqp.split_tests_to_groups(test_names, self.tests_per_group, self.min_tests_per_group)?;

        println!(
            "Running dEQP on {} threads in {}-test groups",
            rayon_threads,
            if let Some((_, tests)) = groups.get(0) {
                tests.len()
            } else {
                0
            }
        );

        Ok(groups)
    }
}

#[derive(Deserialize)]
struct SuiteConfig {
    deqp: Vec<DeqpConfig>,
}

fn main() -> Result<()> {
    let opts = Opts::from_args();

    match opts.subcmd {
        SubCommand::Run(run) => {
            run.common.setup()?;

            let config = DeqpConfig {
                deqp: run.deqp,
                deqp_args: run.deqp_args,
                caselists: run.caselist,
                baseline: run.common.baseline,
                include: run.common.include,
                timeout: run.common.timeout,
                skips: run.common.skips,
                flakes: run.common.flakes,
                tests_per_group: run.tests_per_group,
                min_tests_per_group: run.min_tests_per_group,
                fraction_start: run.common.fraction_start,
                fraction: run.common.fraction,
                env: run.common.env.into_iter().collect(),
                prefix: "".to_owned(),
                renderer_check: run.renderer_check,
                version_check: run.version_check,
            };
            let deqp = config.deqp(
                &run.common.output_dir,
                run.testlog_to_xml,
                run.common.save_xfail_logs,
            )?;

            let results = parallel_test(std::io::stdout(), config.test_groups(&deqp, &[])?)?;
            process_results(&results, &run.common.output_dir, run.common.summary_limit)?;
        }

        SubCommand::Suite(suite) => {
            suite.common.setup()?;

            let toml_str = std::fs::read_to_string(&suite.suite).context("Reading config TOML")?;
            let suite_config =
                toml::from_str::<SuiteConfig>(toml_str.as_str()).context("Parsing config TOML")?;

            // Apply defaults to the configs.
            let mut configs = Vec::new();
            for mut config in suite_config.deqp {
                if config.tests_per_group == 0 {
                    config.tests_per_group = 500;
                }
                if config.fraction == 0 {
                    config.fraction = 1;
                }
                if config.fraction_start == 0 {
                    config.fraction_start = 1;
                }

                for f in &suite.common.skips {
                    config.skips.push(f.clone());
                }

                for f in &suite.common.flakes {
                    config.flakes.push(f.clone());
                }

                if let Some(run_baseline) = suite.common.baseline.as_ref() {
                    if config.baseline.is_some() {
                        eprintln!(
                            "baseline may only be set on either the command line or per-deqp."
                        );
                        std::process::exit(1);
                    }
                    config.baseline = Some(run_baseline.clone());
                }

                // Apply global fraction to the suite's internal fraction.
                config.fraction *= suite.common.fraction;
                config.fraction_start += suite.common.fraction_start - 1;

                if config.timeout == 0.0 {
                    config.timeout = 60.0;
                }

                for (var, data) in &suite.common.env {
                    config.env.insert(var.to_owned(), data.to_owned());
                }

                configs.push(config);
            }

            let mut deqp = Vec::new();
            for config in &configs {
                deqp.push(config.deqp(
                    &suite.common.output_dir,
                    suite.testlog_to_xml.clone(),
                    suite.common.save_xfail_logs,
                )?);
            }

            let mut test_groups = Vec::new();
            for (config, deqp) in configs.iter().zip(deqp.iter()) {
                let mut groups = config.test_groups(deqp, &suite.common.include)?;
                test_groups.append(&mut groups);
            }

            let results = parallel_test(std::io::stdout(), test_groups)?;
            process_results(
                &results,
                &suite.common.output_dir,
                suite.common.summary_limit,
            )?;
        }

        SubCommand::Junit(junit) => {
            stderrlog::new().module(module_path!()).init().unwrap();

            let results = RunnerResults::from_csv(&mut File::open(&junit.results)?)
                .context("Reading in results csv")?;

            results.write_junit_failures(
                &mut File::create(&junit.output)?,
                &junit.junit_generator_options,
            )?;
        }

        SubCommand::MockDeqp(mock) => {
            stderrlog::new().module(module_path!()).init().unwrap();

            mock_deqp(&mock)?;
        }
    }

    Ok(())
}
