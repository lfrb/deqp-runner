// Copyright (c) 2021 Advanced Micro Devices, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice (including the next
// paragraph) shall be included in all copies or substantial portions of the
// Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

use anyhow::{Context, Result};
use deqp_runner::mock_piglit::{mock_piglit, MockPiglit};
use deqp_runner::piglit_command::PiglitCommand;
use deqp_runner::{
    parallel_test, process_results, CommandLineRunOptions, TestCase, TestCommand, TestConfiguration,
};
use std::path::PathBuf;
use std::time::Duration;
use structopt::StructOpt;

use deqp_runner::parse_piglit::{parse_piglit_xml_testlist, read_profile_file};

#[derive(Debug, StructOpt)]
#[structopt(
    author = "Emma Anholt <emma@anholt.net>",
    about = "Runs piglit in parallel"
)]
struct Opts {
    #[structopt(subcommand)]
    subcmd: SubCommand,
}

#[derive(Debug, StructOpt)]
enum SubCommand {
    #[structopt(name = "run")]
    Run(Run),
    #[structopt(name = "mock-piglit")]
    MockPiglit(MockPiglit),
}

#[derive(Debug, StructOpt)]
pub struct Run {
    #[structopt(long, help = "path to piglit folder")]
    pub piglit_folder: PathBuf,

    #[structopt(long, help = "piglit profile to run (such as quick_gl)")]
    pub profile: String,

    #[structopt(flatten)]
    pub common: CommandLineRunOptions,

    #[structopt(long = "process-isolation")]
    pub process_isolation: bool,
}

fn parse_piglit_xml(
    piglit_folder: &std::path::Path,
    profile: &str,
    process_isolation: bool,
) -> Result<Vec<TestCase>> {
    let test_folder = piglit_folder.join("tests");
    let text = read_profile_file(&test_folder, profile, process_isolation)?;
    parse_piglit_xml_testlist(&test_folder, &text, process_isolation)
}

fn main() -> Result<()> {
    let opts = Opts::from_args();

    match opts.subcmd {
        SubCommand::Run(run) => {
            run.common.setup()?;

            let include_filter = run.common.includes_regex()?;

            let tests: Vec<TestCase> =
                parse_piglit_xml(&run.piglit_folder, &run.profile, run.process_isolation)
                    .with_context(|| format!("reading piglit profile '{}'", &run.profile))?
                    .into_iter()
                    .skip(run.common.fraction_start - 1)
                    .step_by(run.common.fraction)
                    .filter(|x| include_filter.is_match(x.name()))
                    .collect();

            println!(
                "Running {} piglit tests on {} threads",
                tests.len(),
                rayon::current_num_threads()
            );

            let deqp = PiglitCommand {
                piglit_folder: run.piglit_folder,
                profile: run.profile,
                config: TestConfiguration {
                    output_dir: run.common.output_dir.clone(),
                    skips: run.common.skips_regex()?,
                    flakes: run.common.flakes_regex()?,
                    baseline: run.common.baseline()?,
                    timeout: Duration::from_secs_f32(run.common.timeout),
                    env: run.common.env.into_iter().collect(),
                    save_xfail_logs: run.common.save_xfail_logs,
                },
            };
            let results =
                parallel_test(std::io::stdout(), deqp.split_tests_to_groups(tests, 1, 1)?)?;
            process_results(&results, &run.common.output_dir, run.common.summary_limit)?;
        }

        SubCommand::MockPiglit(mock) => {
            stderrlog::new().module(module_path!()).init().unwrap();

            mock_piglit(&mock)?;
        }
    }

    Ok(())
}
